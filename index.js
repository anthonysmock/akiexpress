const
	builder = require('botbuilder'),
	request = require('request'),
	restify = require('restify'),
	settings = require('./settings.js'),
	directory = require('./annuaire.json');

const server = restify.createServer();
server.listen(3978, () => {
	console.log('Listening on port 3978 with appId %s', settings.ChatConnector.appId);
});

const connector = new builder.ChatConnector({
	appId: settings.ChatConnector.appId,
	appPassword: settings.ChatConnector.appPassword
})

const bot = new builder.UniversalBot(connector);
server.post('/', connector.listen());

var os = require("os");

function findexpertwithname(person, type){

  var experts = [];
  var regexfirstname = new RegExp(person, 'i');

  var results;

  for(index in directory)
  {

    if(regexfirstname.test(directory[index].Firstname +  ' ' + directory[index].Lastname) || regexfirstname.test(directory[index].Lastname +  ' ' + directory[index].Firstname))
          experts.push(directory[index]);
  }
  
  if(experts[0] == '' || typeof experts[0] == 'undefined')
  {
    return "Sorry, I cannot find "+ person + " within the company :(" ;
  }
  else
  {
    results = "You can contact "+person+ " with these coordinates : "+ experts[0].Phone;
    return results;
  }
}


function findexpert(domain,place,type)
{
	if(domain == '' || typeof domain == 'undefined')
		return "Sorry, there was an error with the skill you need."
	

//	if(type != '' && typeof type != 'undefined')
	console.log("domain: " + domain + ", place: " + place + ", type :" + type);

	var experts = [];
	var regex = new RegExp('\\b' + domain +'\\b', 'i');
	for(index in directory)
	{
		if(regex.test(directory[index].Skill))
		experts.push(directory[index]);
	}
	
	if(experts[0] == '' || typeof experts[0] == 'undefined')
	{
		// 
			// result += ' around '+place;
		return "Sorry, I cannot find someone with "+ domain +" skills." ;
	}
	
	var results;
	
	if(place != '' && typeof place != 'undefined')//optim: mettre ca dans la premiere passe
	{
		expertsIn = [];
		var regex = new RegExp('\\b' + place + '\\b', 'i');
		
		for(index in experts)
		{
			person = experts[index];
			if(regex.test(person.place))
				expertsIn.push(person);
		}
		
		if(expertsIn[0] == '' || typeof expertsIn[0] == 'undefined')
		{
			res = "Sorry, could not find someone with skills in " + domain + ' around '+ place;
			if(experts.length)
				res += " but " + experts.length + " were found elsewhere"
			
			res += "."
			
			return res;
		}
		
		results = expertsIn;
	} else results = experts;
	
  var answer;
  answer = "You can contact : \t\r";

  for(i=0; i<results.length; i++) 
  {
    answer = answer + "     " + results[i].Firstname + " " + results[i].Lastname + "("+ results[i].place+") : " + results[i].Phone + "\t\r" ;
  }

	return answer;
}

class ApiAiRecognizer {
  constructor(token) { this._token = token; }

  recognize(context, done) {
    const opts = {
      url: 'https://api.api.ai/v1/query?v=20150910',
      headers: { Authorization: `Bearer ${this._token}` },
      json: {
        query: context.message.text,
        lang: 'en',
        sessionId: '0000',
      },
    };

    request.post(opts, (err, res, body) => {
      if (err) return done(err);

      return done(null, {
        score: body.result.score,
        intent: body.result.metadata.intentName,
        entities: body.result.parameters,
      });
    });
  }
}

const recognizer = new   ApiAiRecognizer('dbfdb0cfcd324d4fa821d4f19d91ad14');
const intents = new builder.IntentDialog({
  recognizers: [recognizer],
});

bot.dialog('/', intents);

// WELCOMING
intents.matches('Hello', [
  (session, args) => {
    session.userData['domain'] ='';
    session.userData['information'] ='';
    session.userData['place'] ='';
    session.userData['type'] ='';
    session.userData['Person'] = '';
    session.send(
      'Hello ! I am Aki-Express, your expert finder. How can I help you ?'
    );
  }
]);

// DISCUSSION


intents.matches('expert', [
  (session, args) => {
    if (args.entities.Expert_CategorieType == '')
    {
      if(session.userData['domain'] == '')
      {
        session.userData['place']=args.entities.Place_Param;
        session.userData['type']=args.entities.type_return;
        session.send('In which domain do you need help ?');
      }
      else
      {
        
        var place = args.entities.Place_Param;
        if (place == ''){
          place = session.userData['place'];
        }
        var type = args.entities.type_return;
        if (type == ''){
          type = session.userData['type'];
        }
        session.send(
          findexpert(session.userData['domain'], place, type)
        );
      
      }
    }
    else
    {
      if(session.userData['information'] == 'info'){
        var type = args.entities.type_return;
        if (type == ''){
          type = session.userData['type'];
        }
        session.userData['information'] = '';
        session.send(
          findexpertwithname(args.entities.Expert_CategorieType, type)
        );
      }else{
        console.log("1B");
        session.userData['domain'] = args.entities.Expert_CategorieType;
        var place = args.entities.Place_Param;
        if (place == ''){
          place = session.userData['place'];
        }
        var type = args.entities.type_return;
        if (type == ''){
          type = session.userData['type'];
        }
        session.send(
          findexpert(args.entities.Expert_CategorieType,place,type)
        );
      }
    }
  }
]);

bot.dialog('/findexpert', [
  (session) => {
    builder.Prompts.text(session, 'In which domain do you need help ?');
  },
  (session, results) => {
    session.userData['domain'] = results.response;
    console.log(session.userData['place']);
    session.send(
      findexpert(results.response,session.userData['place'], session.userData['type'])
    );
    session.endDialog();
  }
]);


intents.matches('Information', [
  (session, args) => {
    if (args.entities.Person_param == '')
    {
      if(session.userData['Person'] == '')
      {
        session.userData['information'] = 'info';
        session.userData['type']=args.entities.type_return;
        session.send('Which person do you want contact ?');
      }
      else
      {
        var type = args.entities.type_return;
        session.userData['information'] = '';
        if (type == ''){
          type = session.userData['type'];
        }
        session.send(
          findexpertwithname(session.userData['Person'], type)
        );
      }
    }
    else
    {
       var type = args.entities.type_return;
        session.userData['information'] = '';
        if (type == ''){
          type = session.userData['type'];
        }
      session.userData['Person'] = args.entities.Person_param;
      session.send(
        findexpertwithname(args.entities.Person_param,type)
      );
    }
  }
]);

// TANKS AND GOODBYE
intents.matches('Thanks', [
  (session, args) => {
    session.userData['domain'] ='';
    session.send(
      'Your\'re  welcome :)'
    );
  }
]);

// JOKES

intents.matches('Love', [
  (session, args) => {
    session.send(
      'Sorry, I am already in a relationship with the GPU.',    
      args.entities.Expert_CategorieType
    );
  }
]);

intents.matches('joke', [
  (session, args) => {
    session.send(
      'I am human.',    
      args.entities.Expert_CategorieType
    );
  }
]);
// AUTRE
intents.matches('Default Fallback Intent', [
  (session, args) => {
    session.userData['domain'] ='';
    session.send(
      'I don\'t understand what you said, please try again with differents word \t\r "Insanity is doing the same thing over and over again and expecting different results" -  Narcotiques Anonymes'
    );
  }
]);
