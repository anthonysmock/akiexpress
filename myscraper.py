
from scrapy import Spider, Request
from ..items import PersonItem
from lxml import etree



class MyScraper(Spider):
    name = u'myscraper'


    def start_requests(self):
        yield Request(
            url=u'http://www.aki.nicolasperret.fr/html_annuaire.html',
            callback=self.parse,
        )


    def parse(self, response):
        # Find a list of div which contains a person (use CSS)
        persons_el = response.css('.contact')

        # Browse the list
        for person_el in persons_el:

            # Create a new item
            item = PersonItem()

            # Extract the name of the person (use CSS)
            item['Lastname'] = person_el.css('.Lastname .value::text').extract_first()

            # Extract the firstname  of the person (use CSS + XPath)
            item['Firstname'] = person_el.css('.Firstname .value::text').extract_first()

            # Extract the place of the person (use CSS)
            item['place'] = person_el.css('.place .value::text').extract_first()

            # Extract the phone  of the person (use CSS + XPath)
            item['Phone'] = person_el.css('.Phone .value::text').extract_first()

            # Extract the  language of the person (use CSS)
            item['Language'] = person_el.css('.Language .value li::text').extract()


            # Extract the skill of the person (use CSS + XPath)
            item['Skill'] = person_el.css('.Skill .value li::text').extract()


            # Export the item
            yield item
